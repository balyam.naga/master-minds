import game



def test_mastermind():
    """
    1. Try different answers such as 99999, 0, 53030
    2. What if the input has 10 digits?
    3. What if the input is not a number?

    """
    m = game.Mastermind()
    m._answer = 753
    m._digitsInput = "asd"
    guess_output = m.guessingnumber(10, 0, str(m._answer), str(145))
    expected_output = {"is_True": False, "Bulls": 0, "Cows": 1}
    print(expected_output)
    print(guess_output)

    assert guess_output == expected_output
    print("Test case passed")

    guess_output = m.guessingnumber(10, 0, str(m._answer), str(143))
    expected_output = {"is_True": False, "Bulls": 1, "Cows": 0}
#     print(expected_output)
#     print(guess_output)
    assert guess_output == expected_output
    print("Test case passed")

    guess_output = m.guessingnumber(10, 0, str(m._answer), str(420))
    expected_output = {"is_True": False, "Bulls": 0, "Cows": 0}

    assert guess_output == expected_output
    print("Test case passed")

    #    Raising exception for type digit
#    Comment out checking the exception

    digit_output = game.ui()
    expected_digit_output = "Input Not Valid Exception"

    assert digit_output == expected_digit_output
    print("Test case passed")

#    Raising exception if entered diferrent number of digits as the number of digits should be same
#    comment out when checking the specific exception
#     length_output = game.ui()
#     expected_length_output = "No of digits should be same"
#     assert length_output == expected_length_output
#     print("Test case passed")






#    game.Mastermind().guessit.reset()

test_mastermind()














