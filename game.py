import random


class DigitInvalidException(Exception):
    pass


class Mastermind:
    _answer = 0
    _digitsInput = 0

    def createrandomnumber(self, a):
        start = 10**(a-1)
        end = 10 ** a
        a = random.randint(start, end)
        return a

    def guess(self, a=10):
        return a


    def guessingnumber(self, number_of_guesses, number_of_guesses_track, random_string, player_guess):
        bulls = 0
        cows = 0
        for j in range(len(random_string)):
            if random_string[j] == player_guess[j]:
                bulls = bulls + 1
            elif player_guess[j] in random_string[j - 1:] and player_guess[j] != random_string[j]:
                cows = cows + 1
        if bulls == len(random_string):
            return {"is_True": True, "Bulls": bulls, "Cows ": cows}
        else:
            return {"is_True": False, "Bulls": bulls, "Cows": cows}




def ui():

     try:
        mastermind = Mastermind()
        a = input("Starting game.... enter no of digits you want to play")
        randomnumber = mastermind.createrandomnumber(int(a))
        guesses = mastermind.guess()
        guesses_track = 0
        random_string = str(randomnumber)
        print(random_string)
        while guesses > 0:
            result = {}
            is_true = False
            player_guess = int(input("enter your number"))
            player_guess = str(player_guess)
            if len(player_guess) != len(random_string):
                raise DigitInvalidException
            result = mastermind.guessingnumber(guesses, guesses_track, random_string, player_guess)
            if result["is_True"] == True:
               print("Yohoo kudos Ultimate guess")
               ui()
            else:
                print(result)
            guesses_track = guesses_track+1
            if guesses_track > guesses:
                guesses = guesses +10
                iscontinue = input("Do you want more guesses or you want to give up?")
                if iscontinue == "N":
                    print("get lost loserr!!!")
            resetorquit = input("Q-->quit,N-->NewGame")
            # if resetorquit == "R":
            #     ui()
            if resetorquit == "Q":
                print("Game over loser HAHAH")
                return
            if resetorquit == "N":
                ui()

     except ValueError:
        return "Input Not Valid Exception"
     except DigitInvalidException:
         return "No of digits should be same"


#print(ui())















